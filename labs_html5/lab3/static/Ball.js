/// <reference path="../node_modules/@types/p5/global.d.ts" />
// @ts-check
"use strict";

/**
 * @param {Number} x - x
 * @param {Number} y - y
 * @param {Number} s - размер
 * @param {Color} c - цвет
 * @param {Number} v - ускорение
 */
function Ball(x, y, s, c, v) {
    
    this.pos = createVector(x, y);
    this.val = createVector(0, v);


    this.s = s;
    this.c = c;

    this.onScreen = true;
}

//Отрисовка
Ball.prototype.draw = function() {

    
    stroke(255, 142, 12);
    strokeWeight(2);
    fill(this.c);

    ellipse(this.pos.x, this.pos.y, this.s);
};

//Обновеление координат шарика
Ball.prototype.update = function() {

    this.pos.y += this.val.y;

    this.onScreen = (this.pos.y < height);
};

//Проверяем, что шарик в корзине
/** 
 * @param {Vector} b - корзина
 */
Ball.prototype.caughtBy = function(b) {

    var x1 = b.x - HALP_B_SIZE;
    var x2 = b.x + HALP_B_SIZE;

    var y2 = b.y + HALP_B_SIZE;

    return !(this.pos.x < x1 || this.pos.x > x2 ||
        this.pos.y < b.y || this.pos.y > y2
    )
}