/// <reference path="../node_modules/@types/p5/global.d.ts" />
// @ts-check
"use strict";

/**
 * @type {Vector}
 */
var basket;
var basketColor;

var orbs = [];

var score;

var mySound;
var mySound1;

//размер корзины
const B_SIZE = 50;
const HALP_B_SIZE = B_SIZE / 2;
var b_coord;
const LEFT_LIMIT_COORD = B_SIZE;
const RIGHT_LIMIT_COORD = 400 - B_SIZE;

function preload() {
    soundFormats('wav',);
    mySound = loadSound('sound81');
    mySound1 = loadSound('sound90');
  }

//Инициализация
function setup() {

    createCanvas(400, 600);
    
    //создание вектора
    basket = createVector(width / 2, height - B_SIZE);
    basketColor = color("#35FFFF");

    score = 0;
    textAlign(CENTER);

    b_coord = width / 2;
}

//Отрисовка
function draw() {
    //Фон
    background(51, 45, 65);

    if (frameCount % 20 === 0 && random() < ((score < 10) ? 10 : score *0.01)) {
        orbs.push(new Ball(random(width), 0, random(20) +10, rCol(), random(5) + 1 ));
        console.log("flag");
        mySound1.setVolume(0.1);
        mySound1.play();
    }

    for(var i = orbs.length - 1; i >= 0; i--) {

        if(orbs[i].onScreen) {

            orbs[i].update();
            orbs[i].draw();

            if (orbs[i].caughtBy(basket)) {
                
                console.log("caught");

                mySound.setVolume(0.1);
                mySound.play();

                score += Math.round(orbs[i].val.y / 2);
                basketColor = lerpColor(basketColor, orbs[i].c, orbs[i].s * 0.01);
                orbs.splice(i, 1);
            }

        } else {
        
            endGame();
        }
    }

    textSize(50);
    noStroke();
    fill(255);
    text(score, width / 2, 50);

    //Смена координат корзины по нажатию клавиши
    if (keyIsDown(LEFT_ARROW)) {
        ChangeBasketCoords(-10);
    }
    if (keyIsDown(RIGHT_ARROW)) {
        ChangeBasketCoords(10);
    }

    handleBasket();
}

function handleBasket() {

    //change basket coord
    basket.x = b_coord;
    //basket.x = width / 2;
    // mouse control
    //basket.x = constrain(mouseX, 0,  width);

    var x1 = basket.x - HALP_B_SIZE;
    var x2 = basket.x + HALP_B_SIZE;

    var y1 = basket.y - HALP_B_SIZE;
    var y2 = basket.y + HALP_B_SIZE;

    //Заливка корзины
    fill(basketColor);
    noStroke();
    rect(x1, basket.y, B_SIZE, HALP_B_SIZE);

    //Цвет и размер кисти
    stroke(255, 192, 45);
    strokeWeight(2);
    noFill();

    //Отрисовка корзины
    beginShape();
    vertex(x1, y1);
    vertex(x1, y2);
    vertex(x2, y2);
    vertex(x2, y1);
    endShape();

}

//нажатие клавиш
function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        ChangeBasketCoords(-10);
    } else if (keyCode === RIGHT_ARROW) {
        ChangeBasketCoords(10);
    }

    //Закончить игру
    if (keyCode === UP_ARROW) {
        endGame();
    }
}

//Проверка и смена координат корзины
function ChangeBasketCoords(different) {
    if (b_coord < LEFT_LIMIT_COORD && different < 0) {
        //
    }
    else if (b_coord > RIGHT_LIMIT_COORD && different > 0) {
        //
    } else {
        b_coord = b_coord + different;
    }

}

//Возвращает случайный цвет
function rCol() {
    return color(random(255), random(255), random(255));
}

//Останавливает игру
function endGame() {
    noLoop();
    textSize(60);
    noStroke();
    fill(255);
    text("Game Over!", width / 2, height / 2);
    console.log("game over");
}
