/// <reference path="../node_modules/@types/p5/global.d.ts" />
// @ts-check
"use strict";

/**
 * @param {Number} x - x
 * @param {Number} y - y
 * @param {Number} size - размер
 * @param {Color} color - цвет
 * @param {Number} velocity - ускорение
 * @param {Color} strokeColor - цвет контура
 */
function Ball(x, y, size, color, velocity, strokeColor) {
    this.position = createVector(x, y);
    this.velocity = createVector(0, velocity);
    this.strokeColor = strokeColor;
    this.size = size;
    this.color = color;
    this.inView = true;
}

//Отрисовка
Ball.prototype.draw = function() {
    stroke(this.strokeColor); //контур
    strokeWeight(2); //ширина контура
    fill(this.color); // заливка шарика
    ellipse(this.position.x, this.position.y, this.size); //шарик
};

//Обновеление координат шарика
Ball.prototype.update = function() {

    this.position.y += this.velocity.y;
    this.inView = (this.position.y < height);
};

//Проверяем, что шарик в корзине
/** 
 * @param {Vector} b - корзина
 */
Ball.prototype.caughtBy = function(basket) {

    var x1 = basket.x - Basket_size /2;
    var x2 = basket.x + Basket_size /2;
    var y2 = basket.y + Basket_size /2;
    //проверка координат
    return !(this.position.x < x1 || this.position.x > x2 ||
        this.position.y < basket.y || this.position.y > y2
    )
}