/// <reference path="../node_modules/@types/p5/global.d.ts" />
// @ts-check
"use strict";

/**
 * @type {Vector}
 */
let basket; //корзина
let colorOfBasket; //цвет корзины
let balls = []; // массив шариков
let score; //счет
 // звук
let mySound;
let mySound1;
//игра запущена
let myLooping = true;
//размер корзины
const Basket_size = 60;
var b_coord;
const LEFT_LIMIT_COORD = Basket_size /2;
const RIGHT_LIMIT_COORD = 400 - Basket_size /2;

const end_game_if_ball_out_off_screen = false;
var backgroundColor;

function preload() {
    //загрузка звука
    soundFormats('wav',);
    mySound = loadSound('sound81');
    mySound1 = loadSound('sound90');
  }

//Инициализация
function setup() {
    //создание холста
    let canvas = createCanvas(400, 600);
    canvas.parent("container");
    //создание вектора для привязки корзины к координатам
    basket = createVector(width / 2, height - Basket_size);
    colorOfBasket = randomColor()
    score = 0;
    textAlign(CENTER);
    b_coord = width / 2;
    backgroundColor = randomColor();
}

//Отрисовка
function draw() {
    myLooping = true;
    //цвет фона
    background(backgroundColor);
    //частота запуска шариков
    let expr = random() < ((score < 1) ? 10 : score *0.75)
    if (frameCount % 22 === 0 && expr) {
        balls.push(new Ball(random(width), 0, random(20) +10, randomColor(), (random(5) + 1) + score * 0.25, randomColor()));
        console.log("b");
        mySound1.setVolume(0.1);
        mySound1.play();
    }

    // для каждого шарика
    for(var i = balls.length - 1; i >= 0; i--) {
        // если шарик на экране
        if(balls[i].inView) {
            
            balls[i].update(); //позиция
            balls[i].draw(); //отрисовка
            
            //шарик в корзине
            if (balls[i].caughtBy(basket)) {
        
                console.log("c");
                mySound.setVolume(0.1);
                mySound.play();
                score += 1;
                balls.splice(i, 1);
            }

        } else {
            if (end_game_if_ball_out_off_screen) {
                endGame();
            }
            
        }
    }

    textSize(30);
    noStroke();
    fill(255);
    text(score, width / 8, 50);

    //Смена координат корзины по нажатию клавиши
    if (keyIsDown(LEFT_ARROW)) {
        ChangeBasketCoords(-10);
    }
    if (keyIsDown(RIGHT_ARROW)) {
        ChangeBasketCoords(10);
    }

    basketController();
}

function basketController() {

    //изменение координат корзины
    basket.x = b_coord;

    var x1 = basket.x - Basket_size /2;
    var x2 = basket.x + Basket_size /2;

    var y1 = basket.y - Basket_size /2;
    var y2 = basket.y + Basket_size /2;

    //Заливка корзины
    fill(colorOfBasket);
    noStroke();
    rect(x1, basket.y, Basket_size, Basket_size /2);

    //Цвет и размер кисти
    stroke(255, 192, 45);
    strokeWeight(2);
    noFill();

    //Отрисовка корзины
    beginShape();
    vertex(x1, y1);
    vertex(x1, y2);
    vertex(x2, y2);
    vertex(x2, y1);
    endShape();

}

//нажатие клавиш
function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        ChangeBasketCoords(-10);
    } else if (keyCode === RIGHT_ARROW) {
        ChangeBasketCoords(10);
    }

    //Закончить игру
    if (keyCode === UP_ARROW) {
        endGame();
    }

    //пауза
    if (keyCode === DOWN_ARROW) {
        pauseGame();
    }
}

//Проверка и смена координат корзины
function ChangeBasketCoords(different) {
    if (b_coord < LEFT_LIMIT_COORD && different < 0) {
        //
    }
    else if (b_coord > RIGHT_LIMIT_COORD && different > 0) {
        //
    } else {
        b_coord = b_coord + different;
    }

}

//Возвращает случайный цвет
function randomColor() {
    return color(random(255), random(255), random(255));
}

//Останавливает игру
function endGame() {
    noLoop();
    textSize(40);
    noStroke();
    fill(255);
    text("Игра закончена!", width / 2, height / 3);
    console.log("g");
}

//pause
function pauseGame() {
    if (myLooping) {
        myLooping = false;
        textSize(40);
        noStroke();
        fill(255);
        text("пауза!", width / 2, height / 3);
        noLoop();
    }
    else {
        loop();
    }
}
