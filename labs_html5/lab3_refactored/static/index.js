
document.addEventListener("DOMContentLoaded", () => {
    var button = document.getElementById("stopGame");
    button.addEventListener("click", function (event) {
        endGame();
    });

    var button1 = document.getElementById("pauseGame");
    button1.addEventListener("click", function (event) {
        pauseGame();
    });

    var button2 = document.getElementById("reloadWindow");
    button2.addEventListener("click", function (event) {
        window.location.reload(false); 
    });

    var minimize = document.getElementById("minimize");
    minimize.addEventListener("click", function (event) {
        $('#controls').slideToggle('slow');
    });
});
