# -*- coding: utf-8 -*-
score = 0
atempt = 0
def run():
    game()

def check(answer, right):
    global score
    global atempt
    if answer.lower() == right.lower():
        print("ответ правильный")
        if atempt == 0:
            score = score + 1
        else:
            score = score + 0.5
        atempt = 0
    else:
        print("ответ не правильный")
        atempt = atempt + 1
        if atempt > 2:
            return
        ans = input("попробуйте еще раз")
        check(ans, right)

def game():
    print("=============GAME=============")
    print(" +=+=+= Вопросы про птиц: ")
    ans1 = input(" Какая лесная птица делает гнездо на земле? ? ")
    check(ans1, "Пеночка")
    ans2 = input("Кто днем спит, ночью летает, прохожих пугает?? ")
    check(ans2, "сова")
    ans3= input("Какие птицы совсем не летают?")
    check(ans3, "Страусы")
    ans4 = input ("Как называется встреча двух петухов??")
    check(ans4, "Петушиный бой")
    print("=-=-=-= итоговая сумма " + str(score))

run()