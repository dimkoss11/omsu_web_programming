from django.apps import AppConfig


class AdminLearningConfig(AppConfig):
    name = 'admin_learning'
