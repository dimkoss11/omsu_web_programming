import datetime
from django.db import models

from django.utils import timezone


class Article(models.Model):
	article_title = models.CharField('название статьи', max_length=200)
	article_author = models.CharField('Автор', max_length=200, default='Автор статьи')
	article_text = models.TextField('текст статьи')
	pub_date = models.DateTimeField('дата публикации', default=timezone.now())

	def __str__(self):
		return self.article_title

	def was_published_recently(self):
		return self.pub_date >= (timezone.now() - datetime.timedelta(days=7))

	class Meta:
		verbose_name='Статья'
		verbose_name_plural='Статьи'

	def __unicode__(self):
		return "%s: %s" % (self.article_author, self.article_title)

	def get_excerpt(self):
		return self.article_text[:140] + "..." if len(self.article_text) > 140 else self.article_text

	get_excerpt.short_description = "Отрывок статьи"


class Comment(models.Model):
	article = models.ForeignKey(Article, on_delete=models.CASCADE)
	author_name = models.CharField('имя автора', max_length=50)
	comment_text = models.CharField('текст комментария', max_length=300)

	def __str__(self):
		return self.author_name

	class Meta:
		verbose_name = 'Комментарий'
		verbose_name_plural = 'Комментарии'
