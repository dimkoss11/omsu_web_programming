from django.contrib import admin

from .models import Article, Comment


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('article_title', 'article_author', 'get_excerpt', 'pub_date')


admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment)
